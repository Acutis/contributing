# Contributing

## New Issues, Bugs, Questions, etc

Before creating any new Issues, please be sure to search through the existing Issues at.... 

Please note there is a code of conduct, please follow it in all your interactions with the project.

## Pull Request Process

1. Fork, then clone the repo:
	`git clone ...`
	NOTE: It is recommended to setup your own branch and commit and merge there initially, using the Master branch for testing before submitting the Pull request.
	If you are part of the main development team, please use the appropriate branch for the changes, or create a new branch off of the Dev Branch.  Only hotfixes should be branched from the master branch.
2. Ensure any install or build dependencies are removed before the end of the layer when doing a build.
3. Update the code making appropriate changes relevant to the issue.  Please keep the Repository clean and clear.
 	Maintain extraneous files.  System, OS and IDE generated files should **not** be tracked and committed and should be added to the `.gitignore` if needed.  This could be maintained locally.
	Do not remove any information from the `.gitignore` without approval.
4. Update the `CHANGELOG.md` with the version, date, released status, user and simple notes on what has changed.  For consistency and ease of reading, please keep the same format.  
	Also, update the `README.md` with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
	If needed, make sure to update the `TODO.md` or any feature list or task list with the correct actions.  
	Ensure that the created ticket that reflects the updates has been marked to either a "review" status or a "closed" status depending on if the changes have been approved and merged.
5. Increase the version numbers in any examples files and the `README.md` to the new version that this Pull Request would represent. 
	The versioning scheme we use is [SemVer](http://semver.org/) ([http://semver.org/](http://semver.org/)).
6. Commit the changes, writing good and clear commit messages.
7. Push to your fork.
8. Create a [pull request](https://github.com/). 
9. You may merge the Pull Request once you have the sign-off of the other developers, or if you do not have permission to do that, you may request a senior developer to merge it for you.
	If you are part of the main development team, and this is a branch, merge the branch into the DEV branch and submit a pull request.  You may merge it yourself after recieving documented approval.

NOTE:  At this time, your pull request is waiting on us to approve the changes.  It may take a few days, please be patient, though we strive to get to all requests within 3-5 days.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, gender, disability, ethnicity, experience, nationality, race, or religion.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Be welcoming and helpful to everyone in the community and anyone whom the community supports
* Being respectful of viewpoints and experiences
* Politey accepting of constructive criticism
* Focusing on what is best for the community and project

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or advances
* Personal and private discussions in this public and community setting
* Trolling, insulting/derogatory comments, and personal attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic address regardless of permission
* Other conduct which could reasonably be considered inappropriate in a professional setting

## Commits  

### Format  

```
<type>: <work status - optional> - <subject - max 100 characters>  

<body - description/summary>  

<footer - including resolved issues - optional>  
```  

Start the first line of each commit with the type (see below) and a brief subject.  Something that might be able to be visually grouped or visually connected to a subject of a task or issue.  
Emojis are discouraged, but not disallowed, though they are fun and the use of them are to the discretion of the project owner.  They should never be required.  

### Types of Commit  

NOTE: Capitialization does not matter.  Just consistency.  

* **feat** :  
    _aka_ - feature  
    New feature (1 per commit please, though multiple commits can be fore each) being added to the application.  
* **fix** :  
    _aka_ - bug, bugfix, hotfix  
    Fixes for errors and bugs seen or reported.  Try to include the issue numbers in the footer.    
* **style** :  
    Feature for style and UI or UX  
* **refractor** :  
    Changing and optimizing code or a section of the codebase, including reformatting notes, spaces, tabs, line feeds, indentation, and names of variables.  
* **test** :  
    _aka_ - qa, qc  
    Any tests being run on the code base.  This could include temporarily creating variables that start the process or to get to a certain condition within the code.   
* **docs** :  
    _aka_ - doc, documentation, document, documents    
    Any and all notes and documentation in code or outside of code.  
* **chore** :  
    _aka_ - task  
    Regular maintenance on the code, such as adding to list of supported languages.  

### Work Status  

NOTE: This is optional, but helpful when there might be several commits for the progress of the code.  

* Init / Initial - Intiall setup and template making.  
* WIP / Dev - Work in Progress or Development.  
* Test - Testing Changes.  
* Live - Changes are made and ready to be pushed live (possibly part of removing test & QA variables).  This should be associated with a tag.  

### Subject & Body  

The subject should be simple and concise.  The subject should be on the first line next to the type of commit and the optional status.  The subject should not end with a period or be a sentance, rather a simple thought.  
The body should be seperated from the subject with a blank line.  The body of the message should be used to explain what changes were made and why.  Original issue and root causes should be stated here as well.  The affected files do not need to be listed.  
Additional paragraphs should be separated by an extra blank line for easability of reading.  Bullet points are encouraged when needed for quick ideas and points.  Code snippets should not be posted here.  
Generally, the body is optional, as some commits and hotfixes don't need a long explaination, but they should still include some description.  This is seen as a form of documentation.  

### Footer  

The footer is optional.  For those using project management software or issue tracking software, the footer should list the issues and tasks it addresses and/or fixes.  

## Issues

The community uses Git / GitLab issues to track public bugs. Please ensure your description is clear and has sufficient instructions to be able to reproduce the issue.

## License

By contributing, you agree that your contributions will be licensed under the LICENSE file in the root directory of this source tree and adhere to the code of conduct, the goal of the project and license agreement.

## Help Improve

See the repository for current documentation, support, tickets and requests.  Please submit a support ticket, or download the source code and contribute.
